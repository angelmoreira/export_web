# Change current attribute Webapp 

## Installation

On src directory use npm to install node_modules 

```bash
npm install
```

## Run
Before running the program, please note that on the file ```MessageHandler.js``` There is a variable at the top of the file ```HARDCODE_DATE``` this variable will be the date of the data export that will be downloaded. **MAKE SURE TO CHANGE IT TO THE DATE DESIRED.**

On ```AzureFunctions.js``` file change the ```azure_connect_string``` variable to point to the Azure storage account you will use

Run the program with:

```node
node app.js
```

## Usage

Once you see the ```App up on: 8080...``` message on the console. You should be able to access the webapp on ```localhost:8080/export```

Simply enter the required information:
* Host
* Space Id/Workspace ID (**NOT the SITE KEY**)
* Client ID & Client Secret(These are the API credentials)

This page may take a moment to load depending on the size of the data export, it will go ahead and download it, unzip it and then looks for the file ```dim-attributes.csv``` Which  will grab all the available attributes in ONE. 

On the next page:
* Select the old attribute that you want to change from the dropdown menu.
* Provide the touchpoint URL that is expecting the new attribute value
* Site key
* And what you want to do with the old attribute. Either make them all upper case, lower case or remove the last character **You have to provide the character you want to remove, which will remove only the last character if it finds a match as that last character**

Click ```CHANGE ATTRIBUTE``` button and the webapp will send to the Azure queue the new attributes.

An Azure function will read from the Queue and send the new values using the lightweight Events API to ONE.

**NOTE, if you want to send the request directly from the webapp and skip the Azure queue you can change file ```FileHandler.js``` line 129 to use the method ```sendToOne()``` from ```Request```. Be sure to troubleshoot the code before just trying it**  

