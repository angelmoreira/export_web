var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var session = require('express-session');

// var mongoose = require('mongoose');
// var MongoStore = require('connect-mongo')(session);
// var mongoConnectionStr = `mongodb://${(process.env.MONGO_HOST || "localhost:27017")}/ExportDB`;
// mongoose.connect(mongoConnectionStr,{ useNewUrlParser: true });
// var db = mongoose.connection;
// db.once('open', function(){
//    console.log("Mongoose connected to " + mongoConnectionStr );
// });
// db.on('error', console.error.bind(console, 'connection error: '));
var hbs = require('hbs');

hbs.registerHelper("inc", function(value, options)
{
    return parseInt(value) + 1;
});

//Setting Node engine to use Handlebars
app.set('view engine', 'hbs');
app.use("/export", express.static("public"));

var server = app.listen(process.env.PORT || 8080, function() {
    console.log('App up on: %s...', server.address().port);
});

//
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(session({
    secret: "hBuatqgRHdgaU!63Mx",
    resave: true,
    saveUninitialized: false
    // ,
    // store: new MongoStore({
    //     mongooseConnection: db
    // })
}));

app.use(require("./components/routes"));

