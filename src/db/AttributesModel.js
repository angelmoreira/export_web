var mongoose = require("mongoose");

var AttributesModel = new mongoose.Schema({
    userId:{
        type: String
    },
    attributes:[{
        type: String
    }],
    spaceId:{
        type: String
    }
},{
    strict: false
});

var Attributes = mongoose.model('Attributes', AttributesModel);

module.exports = Attributes;