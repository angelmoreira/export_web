let AdmZip = require('adm-zip');
let fs = require('fs');
let archiver = require('archiver');
const intoStream = require('into-stream');
let csv = require('csvtojson').Converter;

let AzureStorage = require('./AzureFunctions');
let Azure;

function FileHandler() {
    Azure = new AzureStorage();
}

FileHandler.prototype.handleFiles = function (data, date, fileContext, callback) {

    let dataAdm = this.unzipFiles2(data);
    if (dataAdm) {
        var date = new Date().toISOString().split("T")[0].replace(/-/g,"");
        this.getAttributes(dataAdm, date, function (attributes) {
            if (attributes) {
                callback(attributes);
            }
        });

    }
    else {
        console.log("Error unzipping file")
        return false;
    }
};

FileHandler.prototype.unzipFiles = function (data) {
    let dataExport = new AdmZip(data);

    return dataExport.getEntries();
};
FileHandler.prototype.unzipFiles2 = function (data) {
     let dataExport = new AdmZip(data);
    //let dataExport = new AdmZip(__dirname + data);

    return dataExport.getEntries();
};

FileHandler.prototype.getAttributes = function (data, name, callback) {

    var dataAttributes = [];

    data.forEach(function (zipEntry) {
        if (zipEntry.entryName == `dim-attributes.csv`) {
            var tempArray = zipEntry.getData().toString('utf8').replace(/\n/g, ",").split(",");
            var count3 = 0;

            var dataAttributes2 = [];

            for (var index = 3; index < tempArray.length; index++) {

                if (count3 <= 3) {

                    dataAttributes2.push(tempArray[index]);
                    count3++;

                    if (count3 >= 3) {
                        dataAttributes.push(dataAttributes2);
                        dataAttributes2 = [];
                        count3 = 0;
                    }
                }
            }
            callback(dataAttributes);
        }
    });
};



FileHandler.prototype.unzipAndSendCustomerAttributes = function (customerAttributeBuffer, tenant, callback) {

    let unzippedData = this.unzipFiles(customerAttributeBuffer);

    unzippedData.forEach(function(zipEntry){

       if (zipEntry.entryName == `customer-attributes.csv`) {

            var converterObj = new csv({delimiter: ','});
            converterObj
                .fromStream(intoStream(zipEntry.getData()))
                .subscribe(function(singleJsonLine){
                    if(singleJsonLine["Attribute ID"] == tenant.changeAtt){
                        tenant.tid = singleJsonLine['TID'];
                        tenant.oldAttribute = singleJsonLine["Text Value"];
                        switch(tenant.case){
                            case 'on':
                                (singleJsonLine["Text Value"].slice(-1) === tenant.trailingCharacter) ?
                                    tenant.attributeValue = singleJsonLine["Text Value"].slice(0, -1) :
                                    tenant.attributeValue = false;
                                break;
                            case 'uppercase':
                                console.log("uppercase")
                                tenant.attributeValue = singleJsonLine["Text Value"].toUpperCase();
                                break;
                            case 'lowercase':
                                tenant.attributeValue = singleJsonLine["Text Value"].toLowerCase();
                                break;
                        }

                        var oneHeader = {
                            'Accept': 'application/json',
                            'Cache-Control': 'no-cache',
                            'Content-Type': 'application/json'
                        };

                        var oneRequestBody = {};

                        var oneUrl = 'https://' + tenant.host + '.thunderhead.com/one/rt/track/'
                            + tenant.newSpace  + '/events?tid=' + tenant.tid;

                        //BUILDING body for ONE
                        oneRequestBody.customerKeyName = tenant.attributeName;
                        oneRequestBody.customerKey = tenant.attributeValue;
                        oneRequestBody.uri =  tenant.baseTouchpointUri;

                        var message = {
                            url: oneUrl,
                            body: oneRequestBody,
                            headers: oneHeader
                        };

                        if(tenant.attributeValue){
                            Azure.sendMessage(message);
                        }
                    }
                })
                .then(function(){
                    console.log("Done reading file");
                    callback(null, true);
                });
        }
    });

};

FileHandler.prototype.changeName = function (name, fileMask, nameFormat, date, extension) {
    switch (nameFormat) {
        case "0":
        case "2":
            return format(fileMask, {
                name: name
            }) + extension;
            break;
        case "1":
        case "4":
            return format(fileMask, {
                name: name,
                timestamp: date
            }) + "." + extension;
            break;
    }
};


FileHandler.prototype.deleteFiles = async function (newFile) {
    fs.unlink(`${newFile}`, (error) => {
        if (error) console.log(error);
    });
    return true;
};

module.exports = FileHandler;