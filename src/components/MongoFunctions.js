var Attributes = require('../db/AttributesModel');
var uidGenerator = require('node-unique-id-generator');

function MongoFunctions(){}

MongoFunctions.prototype.saveAttributes = function(attributes, userId, callback){
  var query = {'attributesId': uidGenerator.generateGUID()}
  Attributes.findOneAndUpdate(query, attributes, {upsert:true, new:true}, function(error, result){
      error ? callback(error) : callback(null, result);
  });
};

module.exports = MongoFunctions;