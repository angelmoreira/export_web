var express = require('express');
var router = express.Router();

router.use('/export', require('./home'));

module.exports = router;