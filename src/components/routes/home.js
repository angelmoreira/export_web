var express = require('express');
var router = express.Router();

var MessageHandler = require('../MessageHandler');

var Handler = new MessageHandler();


router.get('/', function(req, res){

    res.render("index.hbs", {layout:"layouts/app.hbs"});

});

router.post('/', async function(req, res){
    //For future use
    //req.body.userId = req.session.userId;
    req.body.userId = "123512441212134";

    Handler.initialHandling(req.body, function(data){
        if(data) {

            var outMsg = {
                attributes: data.attributes,
                tenant: data.tenant,
                statusCode: 200
            };
            outMsg.layout = "layouts/app.hbs";
            console.log("Finished process sending to hbs ");
            res.render("index.hbs", outMsg);
        }
        else{
            console.log("Error occurred ", error);
            res.send(error);
        }
    })
    .catch((error) => {
        console.log("Error ", error);
    });

});

router.post('/attribute', function(req, res){

    Handler.changeAttribute(req.body, function(data){
        if(data) {
            var outMsg = {
                layout: "layouts/app.hbs",
                success: data,
                statusCode: 200
            };
            res.render('index.hbs', outMsg);
        }
        else{
            console.log("ERROR!!");
        }
    });
});


router.post('/test', function(req, res){
    var util = require('util');
    console.log(util.inspect(req.body, false, null));
})

module.exports = router;