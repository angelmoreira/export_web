let btoa = require("btoa");
let fetch = require("node-fetch");

function AuthFunctions(){}

AuthFunctions.prototype.getOauth2Token = async function (tenant) {

    let auth = btoa(`${tenant.clientId}:${tenant.clientSecret}`);
    let response = await fetch(`https://${tenant.host}.thunderhead.com/one/oauth2token`, {
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "authorization": `Basic ${auth}`,
            "Access-Control-Allow-Origin": "*"
        },
        "body": "grant_type=client_credentials"
    });
    let result = await response.json();
    return result;
};

module.exports = AuthFunctions;