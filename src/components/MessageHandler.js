var FileHandler = require('./FileHandler');
var Request = require('./Request');

var FileHandler = new FileHandler();
var requestFunc = new Request();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////             MANUAL DATE IN CASE YOU NEED AN EXPORT FROM A SPECIFIC DATE          /////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
var HARDCODE_DATE = "20181211";

function MessageHandler() {}

MessageHandler.prototype.initialHandling = async function (data, callback) {
    try {
        //Initializing variables from POST body
        var tenant = {
            host: data.host,
            clientId: data.clientId,
            clientSecret: data.clientSecret,
            spaceId: data.spaceId
        };
        var fileContext = data.fileContext;
        //Future use
        var userId = data.userId;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////             MANUAL DATE IN CASE YOU NEED AN EXPORT FROM A SPECIFIC DATE          /////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let searchDate = HARDCODE_DATE || new Date().toISOString().split("T")[0].replace(/-/g, "");


        requestFunc.downloadExport(tenant, searchDate, function(outputData, tenant){
            if (outputData) {
                try {
                    FileHandler.handleFiles(outputData, searchDate, fileContext, function (result) {
                        if (result) {
                            let MongoFunctions = require('./MongoFunctions');
                            var mongo = new MongoFunctions();
                            var attributesToSave = {
                                attributes: result,
                                tenant: tenant
                            };

                            callback(attributesToSave);

                        }
                    });
                }
                catch (error) {
                    console.log("Error occurred: " + error);
                    callback(error);
                }
            }
            else {
                console.log("outputData appears empty: ", outputData);
            }
        })
        .catch(function(error){
            console.log("Error in downloadExport function: ", error);
        });


        // }else{
        //     //There was no extract for the date provided.
        //     return "No extract available for date provided";
        // }


    }
    catch (e) {
        console.log("Error: ", e)
    }
};

MessageHandler.prototype.changeAttribute = function (data, callback) {

    var tenant = {
        host: data.host,
        clientId: data.clientId,
        clientSecret: data.clientSecret,
        spaceId: data.spaceId,
        exportId: data.exportId,
        changeAtt: data.changeAtt.split(";")[0],
        attributeName: data.changeAtt.split(";")[1],
        trailingCharacter: data.trailingCharacter,
        case: data.case,
        baseTouchpointUri: data.uri,
        newSpace: data.newSpace,
        newAtt: data.newAtt
    };

    tenant.attributeName = tenant.attributeName.toLowerCase();
    console.log(tenant.attributeName);


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////             MANUAL DATE IN CASE YOU NEED AN EXPORT FROM A SPECIFIC DATE          /////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let searchDate = HARDCODE_DATE || new Date().toISOString().split("T")[0].replace(/-/g, "");

    requestFunc.downloadExport(tenant, searchDate, function(outputData){

        FileHandler.unzipAndSendCustomerAttributes(outputData, tenant, function (error, results) {
        //FileHandler.unzipAndSendCustomerAttributes("/zipped.zip", tenant, function (error, results) {
            if (error) {
                console.log("Error on unzipAndSendCustomerAttributes ", error)
            }
            else {
                console.log("No errors ", results);
                callback("No errors unzipping and sending file, the process will take a few minutes or more depending on" +
                    " size of the file");
            }
        });
    })
    .catch(function(error){
        console.log("Error in downloadExport function: ", error);
    });



};

module.exports = MessageHandler;
