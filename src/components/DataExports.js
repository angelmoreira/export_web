let fetch = require("node-fetch");
let download = require("download");

function DataExports(){}

DataExports.prototype.getDataExports = async function(auth, tenant){

    let response = await fetch(`https://${tenant.host}.thunderhead.com/one/oauth2/batch/${tenant.spaceId}/dataexport`, {
        "headers": {
            "authorization": auth
        }
    });
    let result = await response.json();
    if (!result) {
        return false
    }
    console.log("data exports: ", result);
    return result;
};

DataExports.prototype.getDataExportDownload = async function(auth, exportId, tenant){

    let response = await fetch(`https://${tenant.host}.thunderhead.com/one/oauth2/batch/${tenant.spaceId}/dataexport/securelink/${exportId}`, {
        "headers": {
            "authorization": auth
        }
    });
    let result = await response.json();
    if (!result) {
        return false
    }
    console.log("today export: ", result);
    return result;
};

DataExports.prototype.downloadDataExport = async function(downloadLink){

    var outData = {};
    await download(downloadLink.secureLink)
        .then(data => {
            outData = data;
            return data;
        })
        .catch(error => {console.log("Error: ", error)});
    if (!outData) {
        return false
    }
    return outData;

};

module.exports = DataExports;