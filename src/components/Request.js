var AuthFunctions = require('./AuthFunctions');
var DataExports = require('./DataExports');

var Oauth = new AuthFunctions();
var DataExport = new DataExports();

var request = require('request');

function Request(){}

Request.prototype.downloadExport = async function(tenant, searchDate, callback) {

    //Grabbing and storing valid OAuth2 token
    let token = await Oauth.getOauth2Token(tenant);
    let oauth2 = `${token.token_type} ${token.access_token}`;

    //Grab the data exports available right now
    let dataExports = await DataExport.getDataExports(oauth2, tenant);

    //Grab the date to be used for the export
    let todayExport = dataExports.find(myExport =>
        (myExport["endDate"]).split("T")[0].replace(/-/g, "") === searchDate);

    tenant.exportId = todayExport.exportId;
    //Download today's export
    if(todayExport) {
        let downloadLink = await DataExport.getDataExportDownload(oauth2, todayExport.exportId, tenant);

        var outputData = await DataExport.downloadDataExport(downloadLink);

        outputData ? callback(outputData, tenant) : callback(null);
    }
};

//Not being used, sending to Queue instead
Request.prototype.sendToOne = async function(tenant){

    var oneRequestBody = {};

    var oneUrl = 'https://' + tenant.host + '.thunderhead.com/one/rt/track/'
        + tenant.newSpace  + '/events?tid=' + tenant.tid;

    //BUILDING body for ONE
    oneRequestBody.customerKeyName = tenant.attributeName;
    oneRequestBody.customerKey = tenant.oldAttribute;
    oneRequestBody.uri =  tenant.baseTouchpointUri;
    oneRequestBody.properties = [{
        "name": tenant.newAtt,
        "value": tenant.attributeValue
    }];

     console.log(oneUrl, " \n", oneRequestBody);
    // request({
    //     method: 'POST',
    //     url: oneUrl,
    //     headers: oneHeader,
    //     body: JSON.stringify(oneRequestBody)
    // }, function(error, response, body) {
    //     if (error) {
    //         console.log("-----] Error: " + error);
    //         Request.prototype.sendToOne(tenant)
    //             .catch(function(error){
    //                 console.log("Error retrying request: ", error);
    //             });
    //         //return (error);
    //     }
    //     else {
    //         try{
    //             var oneMessage = JSON.parse(body);
    //             if (oneMessage.statusCode != 200) {
    //
    //             }
    //             else {
    //                 // Error Handling
    //                 console.log("Response from ONE:", oneMessage)
    //                 return oneMessage;
    //             }
    //         }
    //         catch(error){
    //             // Request.prototype.sendToOne(tenant)
    //                 // .catch(function(error){
    //                     console.log("Error retrying request: ", error);
    //                     return ("Error retrying to send request ", error);
    //                 // });
    //         }
    //
    //     }
    // });
};

module.exports = Request;